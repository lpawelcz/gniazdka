package pc;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

public class OrderPane extends JPanel {
	
	JTextArea chat = new JTextArea();
	
	public OrderPane() {

		JPanel chatPanel = new JPanel(new GridLayout());
		chatPanel.add(new JScrollPane(chat));
		chat.setEditable(false);
		
		add(chatPanel, BorderLayout.CENTER);
		setBackground(Color.white);
	
	}
	void append(String str) {
		chat.append(str);
		chat.setCaretPosition(chat.getText().length() - 1);
	}
}