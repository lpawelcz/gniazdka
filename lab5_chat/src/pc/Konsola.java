package pc;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class Konsola {

	private static int mojeID;

	private ArrayList<ClientThread> al;

	private SimpleDateFormat sdf;

	private int port;

	private boolean run;

	public Konsola(int port) {

		this.port = port;
		sdf = new SimpleDateFormat("HH:mm:ss");
		al = new ArrayList<ClientThread>();
	}

	public void start() {
		run = true;

		try {

			ServerSocket serverSocket = new ServerSocket(port);
			InetAddress ip = InetAddress.getLocalHost();

			while (run) {

				wyswietl("Serwer oczekuje na uzytkownik�w, na:  " + ip + ":" + port + ".");

				Socket socket = serverSocket.accept();

				if (!run)
					break;
				ClientThread t = new ClientThread(socket);
				al.add(t);
				t.start();
			}

			try {
				serverSocket.close();
				for (int i = 0; i < al.size(); ++i) {
					ClientThread tc = al.get(i);
					tc.sInput.close();
					tc.sOutput.close();
					tc.socket.close();

				}
			} catch (Exception e) {
				wyswietl("wyjatek przy zamykaniu:" + e);
			}
		}

		catch (IOException e) {
			String msg = sdf.format(new Date()) + " cos nie pyklo: " + e + "\n";
			wyswietl(msg);
		}
	}

	private void wyswietl(String msg) {
		String czas = sdf.format(new Date()) + " " + msg;

		System.out.println(czas);

	}
	// Nadawanie do wszystkich

	private synchronized void nadaj(String message) {

		String czas = sdf.format(new Date());
		String messageLf = czas + " " + message + "\n";

		System.out.print(messageLf);

		for (int i = al.size(); --i >= 0;) {
			ClientThread ct = al.get(i);

			if (!ct.writeMsg(messageLf)) {
				al.remove(i);
				wyswietl("odlaczono  " + ct.nick + " i usunieto z listy.");
			}
		}
	}

	synchronized void remove(int id) {

		for (int i = 0; i < al.size(); ++i) { // wylogowywanie
			ClientThread ct = al.get(i);
			if (ct.id == id) {
				al.remove(i);
				return;
			}
		}
	}

	// FUNKCJA G��WNA

	public static void main(String[] args) {

		int port = 1500;
		Konsola server = new Konsola(port);
		server.start();
	}

	// w�tki dla klient�w

	class ClientThread extends Thread {

		Socket socket;
		ObjectInputStream sInput;
		ObjectOutputStream sOutput;

		String data; // data pod��czenia
		String nick;
		int id;
		ChatMessage cm;

		ClientThread(Socket socket) {
			id = ++mojeID;
			this.socket = socket;

		//	System.out.println("watek tworzy strumien wejscia i wyjscia");
			try {

				sOutput = new ObjectOutputStream(socket.getOutputStream());
				sInput = new ObjectInputStream(socket.getInputStream());

				nick = (String) sInput.readObject();
				wyswietl("polaczono: " + nick);
			} catch (IOException e) {
				wyswietl("nie udalo sie stworzy� strumieni: " + e);
				return;
			}

			catch (ClassNotFoundException e) {
			}
			data = new Date().toString() + "\n";
		}

		public void run() {

			boolean run = true;
			while (run) {

				try {
					cm = (ChatMessage) sInput.readObject(); // czytanie
				} catch (IOException e) {
					wyswietl(nick + " wyjatek podczas czytania strumienia: " + e);
					break;
				} catch (ClassNotFoundException e2) {
					break;
				}

				String message = cm.getMessage();

				switch (cm.getType()) {
				case ChatMessage.MESSAGE:
					nadaj(nick + ": " + message);
					break;
				case ChatMessage.LOGOUT:
					wyswietl(nick + " wylogowal sie procedura LOGOUT");
					run = false;
					break;
				case ChatMessage.LISTA:
					writeMsg("Podlaczeni uzytkownicy " + sdf.format(new Date()) + "\n");

					for (int i = 0; i < al.size(); ++i) {
						ClientThread ct = al.get(i);
						writeMsg((i + 1) + ") " + ct.nick + ", nieprzerwanie od " + ct.data);
					}
					break;
				}
			}
			remove(id);
			close();
		}

		private void close() {
			try {
				if (sOutput != null)
					sOutput.close();
			} catch (Exception e) {
			}
			try {
				if (sInput != null)
					sInput.close();
			} catch (Exception e) {
			}
			;
			try {
				if (socket != null)
					socket.close();
			} catch (Exception e) {
			}
		}

		// pisanie
		private boolean writeMsg(String msg) {

			if (!socket.isConnected()) {
				close();
				return false;
			}

			try {
				sOutput.writeObject(msg);
			}

			catch (IOException e) {
				wyswietl("wysylanie wiadomosci do " + nick + "zakonczone niepowodzeniem");
				wyswietl(e.toString());
			}
			return true;
		}
	}
}
