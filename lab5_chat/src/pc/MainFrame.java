package pc;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Color;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	JLabel lblNotConnected;
	JTextField ip;
	JTextField port;
	JTextField nick;
	Client client;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHost = new JLabel("Ip:");
		lblHost.setBounds(10, 14, 35, 14);
		contentPane.add(lblHost);
		
		ip = new JTextField();
		ip.setBounds(43, 11, 90, 20);
		contentPane.add(ip);
		
		JTextField port = new JTextField();
		port.setBounds(43, 39, 90, 20);
		contentPane.add(port);
		
		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 42, 35, 14);
		contentPane.add(lblPort);
		
		JTextField nick = new JTextField();
		nick.setBounds(43, 70, 90, 20);
		contentPane.add(nick);
		
		JLabel lblNick = new JLabel("Nick:");
		lblNick.setBounds(10, 70, 35, 14);
		contentPane.add(lblNick);
		
		JButton btnConnect = new JButton("Connect");
		btnConnect.setBounds(20, 95, 100, 23);
		contentPane.add(btnConnect);
	
		lblNotConnected = new JLabel("Not Connected");
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 120, 123, 23);
		contentPane.add(lblNotConnected);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.setBounds(20, 160, 100, 23);
		contentPane.add(btnLogout);
		
		OrderPane panel = new OrderPane();
		panel.setBounds(145, 14, 500, 500);
		contentPane.add(panel);
		
		
		
		btnConnect.addActionListener(new ActionListener() {

			
			public void actionPerformed(ActionEvent e) {
				client.sendMessage(new ChatMessage(ChatMessage.LOGOUT, "")); 
			
			}
			
		});
		
		
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
					
				Client client = new Client(ip.getText(), Integer.parseInt(port.getText()), nick.getText(),panel);
					if (!client.start())
						return;
					
					Scanner scan = new Scanner(System.in);


					lblNotConnected.setText("Connected");
					lblNotConnected.setForeground(Color.BLACK);
					lblNotConnected.setBackground(Color.GREEN);
					
					
					while (true) {
						System.out.print("> ");
						String msg = scan.nextLine();

						if (msg.equalsIgnoreCase("LOGOUT")) {
							client.sendMessage(new ChatMessage(ChatMessage.LOGOUT, "")); // wyjscie, komenda LOGOUT
							break;
						}

						else if (msg.equalsIgnoreCase("LISTA")) { // Lista podlaczonych
							client.sendMessage(new ChatMessage(ChatMessage.LISTA, ""));
						} else {
							client.sendMessage(new ChatMessage(ChatMessage.MESSAGE, msg));
						}
					}

					client.disconnect();
					scan.close();
				
				
			}
		});

}
	

}
