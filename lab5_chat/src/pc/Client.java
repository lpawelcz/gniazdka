package pc;

import java.net.*;
import java.io.*;

public class Client {

	private String ip, nick;
	private int port;

	private Socket socket;
	private ObjectOutputStream sOutput;
	private ObjectInputStream sInput;
	
	private OrderPane chatbox;

	Client(String server, int port, String username, OrderPane chatbox) {
		this.nick = username;
		this.ip = server;
		this.port = port;
		this.chatbox = chatbox;

	}
	
/*	// FUNKCJA G��WNA

		public static void main(String[] args) {

			Scanner sc = new Scanner(System.in);
			
			System.out.println("nick: ");
			String nick = sc.nextLine();
			
			System.out.println("ip: ");
			String ip  = sc.nextLine();
			
			System.out.println("port: ");
			int port  = sc.nextInt();
			
		//	Client client = new Client("localhost", 1500, "pablo");
			Client client = new Client(ip, port, nick);
			
			if (!client.start())
				return;

			Scanner scan = new Scanner(System.in);

			while (true) {
				System.out.print("> ");
				String msg = scan.nextLine();

				if (msg.equalsIgnoreCase("LOGOUT")) {
					client.sendMessage(new ChatMessage(ChatMessage.LOGOUT, "")); // wyjscie, komenda LOGOUT
					break;
				}

				else if (msg.equalsIgnoreCase("LISTA")) { // Lista podlaczonych
					client.sendMessage(new ChatMessage(ChatMessage.LISTA, ""));
				} else {
					client.sendMessage(new ChatMessage(ChatMessage.MESSAGE, msg));
				}
			}

			client.disconnect();
			scan.close();
			sc.close();
		}*/
	
	public boolean start() {

		try {
			socket = new Socket(ip, port);
		}

		catch (Exception ec) {
			wyswietl("nieudane laczenie z serwerem:" + ec);
			return false;
		}

		String msg = "polaczono z: " + socket.getInetAddress() + ":" + socket.getPort();
		wyswietl(msg);

		try {
			sInput = new ObjectInputStream(socket.getInputStream());
			sOutput = new ObjectOutputStream(socket.getOutputStream());
		} catch (IOException eIO) {
			wyswietl("wyjatek przy tworzeniu strumieni: " + eIO);
			return false;
		}

		new ServerListener().start(); // watek do nasluchiwania serwera

		try {
			sOutput.writeObject(nick);
		} catch (IOException eIO) {
			wyswietl("wyjatek podczas wysylania loginu : " + eIO);
			disconnect();
			return false;
		}

		return true;
	}

	private void wyswietl(String msg) {

		System.out.println(msg); // wyswietlanie wiadomosci
		chatbox.append(msg + "\n");
	}

	void sendMessage(ChatMessage msg) {
		try {
			sOutput.writeObject(msg);
		} catch (IOException e) {
			wyswietl("wyjatek podczas wysylania wiadomosci na serwer: " + e);
		}
	}

	void disconnect() {
		try {
			if (sInput != null)
				sInput.close();
		} catch (Exception e) {
		}
		try {
			if (sOutput != null)
				sOutput.close();
		} catch (Exception e) {
		}
		try {
			if (socket != null)
				socket.close();
		} catch (Exception e) {
		}
	}

	class ServerListener extends Thread {

		public void run() {
			while (true) {
				try {
					String msg = (String) sInput.readObject();

					System.out.println(msg);
					System.out.print("> ");
					chatbox.append(msg);

				} catch (IOException e) {
					wyswietl("Serwer zamknal polaczenie: " + e);
					break;
				} catch (ClassNotFoundException e2) {
				}
			}
		}
	}
}
